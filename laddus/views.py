from django.shortcuts import render,render_to_response
from django.template import RequestContext
from django.http import HttpResponse, Http404, HttpResponseRedirect

from laddus.models import Laddu, UserForm, Secretkey, SmsAlert, EmailAlert, userExtension
from laddus.forms import LadduForm
from django.shortcuts import redirect

from django.core.exceptions import PermissionDenied
from django.core.mail import send_mail
from django.contrib.auth.models import User
from django.contrib.auth.decorators import login_required
from django.contrib.auth import authenticate, login

from django.core.urlresolvers import reverse
from django.http import Http404

import json

import random
import string

from ladduCeleryJobs import checkLaddu

def getJob(id):
	try:
		id=int(id)
		laddus = Laddu.activeObjects.filter(id=id)
	except:
		laddus = Laddu.activeObjects.filter(slug=id)
	
	if laddus.count() != 1:
		return None
	else:
		return laddus[0]





def home(request):
	responce = {}
	if request.user.is_authenticated():
		
		responce['userExtn'] = userExtension(request.user)
		
		return render_to_response('Home.html',responce,context_instance=RequestContext(request))
	return render_to_response('homepage.html',responce,context_instance=RequestContext(request))

def account_profile(request):
	if request.user.is_authenticated():
		return redirect('/')
	else:
		return redirect('/accounts/login/')
def userRegister(request):
	return render_to_response('userRegisterForm.html',context_instance=RequestContext(request))
#def saveUser(request):
	#if request.method == "POST":
		#userName = request.POST['username']
		#userPass = request.POST['password']
		#userMail = request.POST['email']
		#if User.objects.filter(username=userName).exists():
			#return HttpResponse("found");
		#else:
			#user = User.objects.create_user(username=userName,email=userMail,password=userPass)
			#return HttpResponse("saved");
	#else:
		#return HttpResponse("fails");


def saveUser(request):
	responce = {}
	if request.method == "POST":
		form = UserForm(request.POST)
		if form.is_valid():
			new_user = User.objects.create_user(**form.cleaned_data)
			username = request.POST['username']
			password = request.POST['password']
			user = authenticate(username=username, password=password)
			login(request, user)
			# redirect, or however you want to get to the main view
			return HttpResponse('saved')
	else:
		form = UserForm()
	responce['data'] = {'form':form.errors}

	respJson = json.dumps(responce)
	return HttpResponse( respJson, content_type = 'application/json' )


def forgotPasswordView(request):
	return render_to_response('forgotPassword.html',context_instance=RequestContext(request))

def forgotPassword(request):
	if request.method == "POST":
		username = request.POST['username']
		username_not_found = False
		if User.objects.filter(username=username):
			
			u = User.objects.get(username__exact=username)
			email = u.email
			key = ''.join(random.choice(string.ascii_uppercase) for _ in range(6))
			try:
				send_mail('Your laddu key for reset password', 'You can use this link to reset your password..  http://127.0.0.1:8000/reset/password/using?key='+ key+'&username='+ username, 'emperorsarath@gmail.com',[email], fail_silently=False)
			except:
				return HttpResponse('failed');
			k = Secretkey()
			k.username = username
			k.key = key 
			k.save()
			return HttpResponse('saved');
		else:
		
			return HttpResponse('username_not_found');
	else:
		return HttpResponse('failed');
	
	
def resetPasswordUsingKey(request):
	success = False
	try:
		key = request.GET['key']
		username = request.GET['username']
		if Secretkey.objects.filter(username=username,key = key,active=True):
			success = True
			return render_to_response('resetPasswordUsingKey.html',{'success':success,'username':username,'key':key},context_instance=RequestContext(request))
		else:
			return render_to_response('resetPasswordUsingKey.html',{'success':success},context_instance=RequestContext(request))
	except:
		return render_to_response('resetPasswordUsingKey.html',{'success':success},context_instance=RequestContext(request))

	
def savePswdUsingKey(request):
	if request.method == "POST":
		username = request.POST['username']
		key = request.POST['key']
		newpassword = request.POST['password1']
		newpassword2 = request.POST['password2']
	
		form = UserForm(request.POST)
		key_obj =  Secretkey.objects.get(username=username,key = key,active=True)
		if key_obj:
			if newpassword==newpassword2:		
				u = User.objects.get(username__exact=username)
				u.set_password(newpassword)
				u.save()
				key_obj.active = False
				key_obj.save()
				user = authenticate(username=username, password=newpassword)
				login(request, user)
				return HttpResponse('saved');
			else:
				return HttpResponse('password_incorrect');
		
		else:
			
			return HttpResponse('no_key');
			
	else:
		
	        return HttpResponse('failed');

@login_required	
def resetPassword(request):
	return render_to_response('resetPassword.html',context_instance=RequestContext(request))

@login_required	
def resetPasswordSave(request):
	if request.method == "POST":
		success = False
		password_incorrect = False
		newpassword = request.POST['password1']
		newpassword2 = request.POST['password2']
		username = request.user
		if newpassword==newpassword2:
			u = User.objects.get(username__exact=username)
			u.set_password(newpassword)
			u.save()
			user = authenticate(username=username, password=newpassword)
			login(request, user)
			success = True
			return render_to_response('resetPassword.html',{'success':success},context_instance=RequestContext(request))
		else:
			password_incorrect = True
			return render_to_response('resetPassword.html',{'password_incorrect':password_incorrect},context_instance=RequestContext(request))		
	else:
		
	        return render_to_response('resetPassword.html',context_instance=RequestContext(request))
	

def jobs(request):
	laddus = Laddu.activeObjects.filter(owner = request.user)
	responce = {
		'laddus' : laddus 
			}
	return render_to_response('Jobs.html',responce,context_instance=RequestContext(request))

def jobStats(request,id):
	
	laddu = getJob(id)
	
	if not laddu:
		raise Http404
	responce = {
		'laddu' : laddu 
		}
	return render_to_response('JobStatus.html',responce,context_instance=RequestContext(request))


def jobStop(request,id):
	
	laddu = getJob(id)
	
	if not laddu:
		raise Http404
	
	if laddu.owner != request.user:
		raise PermissionDenied()
	
	laddu.runningStatus = False
	laddu.save()
	
	if request.method == "GET":
		return HttpResponseRedirect(reverse('jobs'))
	else:
		response_data = {}
		response_data['result'] = 'success'
		response_data['jobStatus'] = laddu.runningStatus
		return HttpResponse(json.dumps(response_data), content_type="application/json")
	

def jobStart(request,id):
	laddu = getJob(id)
	
	if not laddu:
		raise Http404
	
	if laddu.owner != request.user:
		raise PermissionDenied()
	
	laddu.runningStatus = True
	laddu.save()
	
	if request.method == "GET":
		return HttpResponseRedirect(reverse('jobs'))
	else:
		response_data = {}
		response_data['result'] = 'success'
		response_data['jobStatus'] = laddu.runningStatus
		return HttpResponse(json.dumps(response_data), content_type="application/json")

def jobRemove(request,id):
	laddu = getJob(id)
	
	if not laddu:
		raise Http404
	
	if laddu.owner != request.user:
		raise PermissionDenied()
	
	laddu.active = False
	laddu.save()
	if request.method == "GET":
		return HttpResponseRedirect(reverse('jobs'))
	else:
		response_data = {}
		response_data['result'] = 'success'
		return HttpResponse(json.dumps(response_data), content_type="application/json")

def jobAddSMSAlert(request,id):
	laddu = getJob(id)
	
	if not laddu:
		raise Http404
	
	if laddu.owner != request.user:
		raise PermissionDenied()
	
	phoneNumber = request.GET['number'] if request.method == "GET" else request.POST['number']
	
	
	smsAlert = SmsAlert(laddu = laddu, number = phoneNumber.replace('_',''))     # input mask sometimes add unwated _
	smsAlert.save()
	
	
	if request.method == "GET":
		return HttpResponseRedirect(reverse('jobStats', kwargs={'id':laddu.getSlug()}))
	
	else:
		response_data = {}
		response_data['result'] = 'success'
		return HttpResponse(json.dumps(response_data), content_type="application/json")

def jobDelSMSAlert(request,id,phoneNumber):
	laddu = getJob(id)
	
	if not laddu:
		raise Http404
	
	if laddu.owner != request.user:
		raise PermissionDenied()
	
	SmsAlert.objects.filter(laddu = laddu, number = phoneNumber).update(active = False)     # input mask sometimes add unwated _
	
	if request.method == "GET":
		return HttpResponseRedirect(reverse('jobStats', kwargs={'id':laddu.getSlug()}))
	
	else:
		response_data = {}
		response_data['result'] = 'success'
		return HttpResponse(json.dumps(response_data), content_type="application/json")

def jobAddEMAlert(request,id):
	laddu = getJob(id)
	
	if not laddu:
		raise Http404
	
	if laddu.owner != request.user:
		raise PermissionDenied()
	
	email = request.GET['email'] if request.method == "GET" else request.POST['email']
	
	
	emAlert = EmailAlert(laddu = laddu, email = email)     # input mask sometimes add unwated _
	emAlert.save()
	
	
	if request.method == "GET":
		return HttpResponseRedirect(reverse('jobStats', kwargs={'id':laddu.getSlug()}))
	
	else:
		response_data = {}
		response_data['result'] = 'success'
		return HttpResponse(json.dumps(response_data), content_type="application/json")

def jobDelEMAlert(request,id,email):
	laddu = getJob(id)
	
	if not laddu:
		raise Http404
	
	if laddu.owner != request.user:
		raise PermissionDenied()
	
	EmailAlert.objects.filter(laddu = laddu, email = email).update(active = False)
	
	if request.method == "GET":
		return HttpResponseRedirect(reverse('jobStats', kwargs={'id':laddu.getSlug()}))
	
	else:
		response_data = {}
		response_data['result'] = 'success'
		return HttpResponse(json.dumps(response_data), content_type="application/json")



def addJob(request):
	if request.method == 'POST':
		lForm = LadduForm(request.POST)
		if lForm.is_valid():
			l = Laddu.objects.create(
				owner = request.user,
				protocol = lForm.cleaned_data['protocol'],
				url = lForm.cleaned_data['url'],
				frequency = lForm.cleaned_data['frequency'],
				)
			checkLaddu.delay(l)
			return HttpResponseRedirect(reverse('jobs'))
		else:
			print dict(lForm.errors.items())
	laddus = Laddu.activeObjects.all()
	responce = {
		'laddus' : laddus 
			}
	return render_to_response('addJob.html',responce,context_instance=RequestContext(request))




def homeSubmition(request):
	responce = {}
	if request.method == "POST":
		form = UserForm(request.POST)
		if form.is_valid():
			new_user = User.objects.create_user(**form.cleaned_data)
			username = request.POST['username']
			password = request.POST['password']
			user = authenticate(username=username, password=password)
			login(request, user)
			# redirect, or however you want to get to the main view
		
			Laddu.objects.create(
				owner = request.user,
				protocol = request.POST['protocol'],
				url = request.POST['url'],
				frequency = 30
				)
			l = Laddu.objects.get(owner = request.user,protocol = request.POST['protocol'])
			responce['data'] = {'slug':l.slug,'success':'True'}
			respJson = json.dumps(responce)
			return HttpResponse( respJson, content_type = 'application/json' )
			
			
	else:
		form = UserForm()
	responce['data'] = {'form':form.errors,'success':'False'}

	respJson = json.dumps(responce)
	return HttpResponse( respJson, content_type = 'application/json' )

	