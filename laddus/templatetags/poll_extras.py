# -*- coding: utf-8 -*-
import sys
reload(sys)  # Reload does the trick!
sys.setdefaultencoding('UTF8')

from django import template
register = template.Library()



@register.filter
def statusClass(status):
	sCoptions = {
		'Running' : 'success',
		'Paused' : 'warning',
		'Stopped' : 'warning',
		}
	print status
	return sCoptions[status]


@register.filter
def getTreatment(userId):
	patient=Patient.objects.get(patient__id=userId)
	mtreat = patient.getOnGoingTreatments()
	retStr = ""
	if not mtreat:
		return '<span style="font-style:italic">Keine laufenden Therapien </span>'
	for m in mtreat:
		retStr += unicode(m.name) + '<br>'
	return retStr

@register.filter
def getLatestSE(userId):
	p = Patient.objects.get(patient=User.objects.get(id=userId))
	seList = p.getMySEGrades()

	status = {}
	retStr = ""
	is_Critical = False
	seGrades = []

	distinctGrades = []
	grades = MySideEffect.objects.filter(patient__id=userId, sideeffectsDate=datetime.datetime.today().date(), is_unexpected=False).distinct('sideEffect__sideeffect')

	for grade in grades:
		ses = MySideEffect.objects.filter(patient__id=userId, sideeffectsDate=datetime.datetime.today().date(), sideEffect=grade.sideEffect).order_by('-id')
		distinctGrades.append(ses[0])

	if len(distinctGrades) == len(seList[0][1:]):
		g = 0
		for se in distinctGrades:
			if se.sideeffecttype:
				g = 1 if se.sideeffecttype == se.sideEffect.grade1 and se.sideeffecttype != None else g
				g = 2 if se.sideeffecttype == se.sideEffect.grade2 and se.sideeffecttype != None else g
				g = 3 if se.sideeffecttype == se.sideEffect.grade3 and se.sideeffecttype != None else g
				g = 4 if se.sideeffecttype == se.sideEffect.grade4 and se.sideeffecttype != None else g
				if g > 0:
					retStr = retStr + '<li class="row-fluid"><div class="span9 psideEffect">'+str(se.sideEffect.sideeffect)+':</div>  <div class="span2">'+str(g)+'</div></li>'
					if g > 1:
						is_Critical = True

		status.update({'grade':retStr})
		status.update({'is_Critical':is_Critical})
		return status

	if len(distinctGrades) < len(seList[0][1:]):
		g = 0
		for se in distinctGrades:
			seGrades.append(se.sideEffect.sideeffect.sideeffect)
			g = 1 if se.sideeffecttype == se.sideEffect.grade1 and se.sideeffecttype != None else g
			g = 2 if se.sideeffecttype == se.sideEffect.grade2 and se.sideeffecttype != None else g
			g = 3 if se.sideeffecttype == se.sideEffect.grade3 and se.sideeffecttype != None else g
			g = 4 if se.sideeffecttype == se.sideEffect.grade4 and se.sideeffecttype != None else g

			if g > 0:
				retStr = retStr + '<li class="row-fluid"><div class="span9 psideEffect">'+str(se.sideEffect.sideeffect)+':</div>  <div class="span2">'+str(g)+'</div></li>'
				if g > 1:
					is_Critical = True


	a = []
	i = 1
	for se in seList[0][1:]:
		for s in seList[1:]:
			if i <= len(seList[0]):
				a.append(s[i])
		i = i + 1
		a = filter(lambda a: a != 0, a)
		if a:
			if not se in seGrades:
				retStr = retStr + '<li class="row-fluid"><div class="span9 psideEffect">'+str(se)+':</div>  <div class="span2">'+str(max(a))+'</div></li>'
				if max(a) > 1:
					is_Critical = True

		a = []

	status.update({'grade':retStr})
	status.update({'is_Critical':is_Critical})
	return status



from decimal import *
@register.filter
def drugValue(drug):
	if drug.editableByDoc:
		return 'Veränderlich'
	else :
		return Decimal(drug.value)

@register.filter
def drugValueEditable(drug, user):
	patient = Patient.objects.get(patient=user)
	try:
		treatment = patient.getOngoingTreatment(datetime.datetime.today().date())
		t = Treatmentdrugs.objects.filter(treatment=treatment[0].treatment, drugname=drug.drugname.id, time=drug.time, drugTime=drug.drugTime).order_by('id')
		drug = t[0]
	except:
		pass
	try:
		qty = MyEditedDrugs.objects.get(patient=user, drug=drug.drugname, treatmentDrug=drug)
	except:
		return Decimal(drug.value)
	else:
		return str(int(qty.qty))

@register.filter
def drugValueEditableQty(drug, user):
	patient = Patient.objects.get(patient=user)
	try:
		qty = MyEditedDrugs.objects.get(patient=user, drug=drug.drugname)
	except:
		return Decimal(drug.value)
	else:
			return str(int(qty.qty))

@register.filter
def numberFormat(number):
    return Decimal(number)

@register.filter
def isCriticalPatient(p):
	seList = p.getMySEGrades()

	seValues = []
	a = []
	for se in seList[0][1:]:
		for s in seList[1:]:
			a.append(s[1])

		a = filter(lambda a: a != 0, a)
		if a:
			seValues.append(a[-1])
		a = []

	for value in seValues:
		if value > 1:
			return 1
	return 0