from django.db import models
from django.contrib.auth.models import User
from django.contrib import admin
from  django.utils.text import slugify
from datetime import datetime
from alertCeleryJobs import sendSMS, sendEmail

from django.forms import ModelForm
import pymongo

from pymongo import MongoClient
from bson.json_util import dumps

import time

import json
import uuid
import re

MONGODB_HOST = 'localhost'
MONGODB_PORT = 27017
client = MongoClient(MONGODB_HOST, MONGODB_PORT)
db = client['laddu']
GRAPH_DB = db['ladduGraphData']


STATUS_CHOICES = (
	(1, 'Success'),
	(2, 'Failed'),
)

RUNNING_STATUS_CHOICES = (
	(True, "Running"),
	(False, "Paused"),
	(None, "Stopped"),
)

PROTOCOL_CHOICES = (
	('http://', 'http://'),
	('https://', 'https://'),
)

FREQUENCY_CHOICES = (
	(15,'15 Minutes'),
	(30,'30 Minutes'),
	(60,'1 Hour'),
	(120,'2 Hour'),
	(720, '12 Hour'),
	(1440,'Day(01:00 IST)'),
)


class ActiveObjects(models.Manager):
    def get_queryset(self):
        return super(ActiveObjects, self).get_queryset().filter(active=True).order_by('-id')




class Wallet(models.Model):
	owner = models.ForeignKey(User)
	smsCredits = models.PositiveIntegerField( default = 10 )
	
	active = models.BooleanField(default=True)
	addedOn = models.DateTimeField(auto_now_add=True, default=datetime.now)
	UpdatedOn = models.DateTimeField(auto_now=True, default=datetime.now)
	
	objects = models.Manager()
	activeObjects = ActiveObjects()
	
	
	
	
	
	
class Laddu(models.Model):
	owner = models.ForeignKey(User)
	protocol = models.CharField(choices=PROTOCOL_CHOICES, max_length=10, default='http://')
	url = models.TextField()
	slug = models.SlugField(null=True, blank=True)
	
	frequency = models.PositiveIntegerField(choices = FREQUENCY_CHOICES, default=120)
	runningStatus = models.NullBooleanField(choices=RUNNING_STATUS_CHOICES, default=True)
	
	active = models.BooleanField(default=True)
	addedOn = models.DateTimeField(auto_now_add=True, default=datetime.now)
	UpdatedOn = models.DateTimeField(auto_now=True, default=datetime.now)
	
	objects = models.Manager()
	activeObjects = ActiveObjects()
	
	def getSlug(self):
		return self.slug if self.slug else self.id
	
	def save(self, *args, **kwargs):
		if not self.id:
			self.slug = slugify(str(uuid.uuid4().get_hex().upper()[0:6]) + '-' + re.sub(r'[^\w]', ' ', self.url[:20]))
		super(Laddu, self).save(*args, **kwargs)
	
	
	
	#def __unicode__(self):
		#return unicode(self.url)[:50] + ' ' + str(self.frequency) + 'm'
		
	def isActiveSmsAlert(self):
		return True if SmsAlert.activeObjects.filter(laddu=self).exists() else False
	
	def isActiveEmailAlert(self):
		return True if EmailAlert.activeObjects.filter(laddu=self).exists() else False
	
	def activeAlerts(self):
		alerts  = []
		if self.isActiveEmailAlert():
			alerts.append('sms')
		if self.isActiveEmailAlert():
			alerts.append('email')
		return alerts
	
	def getSMSAlertNumbers(self):
		return SmsAlert.activeObjects.filter(laddu=self)
	
	def getEMAlertNumbers(self):
		return EmailAlert.activeObjects.filter(laddu=self)
	
	def doAlert(self,statusCode,statusReason):
		alerts = self.activeAlerts()
		
		# send sms
		for smsToSend in self.getSMSAlertNumbers():
			if not self.updateWalletSMS():
				break
			
			message = "!!!Website DOWN :"
			message += self.url[:20]
			message += " Status:"
			message += str(statusCode) if statusCode else "NOT OK"
			message += (' (' + str(statusReason) + ')')  if statusCode else ""
			print "sending sms to " + smsToSend.number + ' : ' + message 
			sendSMS.delay(smsToSend.number,message)
		
		# send emails
		emailTos = [ x.email for x in self.getEMAlertNumbers() ]
		subject = "WebSite Down : " + self.protocol + self.url
		message = "Last check for your web site " + self.protocol + self.url + " failed <br>"
		message += "for more details please login to your chick ping dashboard"
		sendEmail.delay( emailTos, subject, message)
	
	def updateWalletSMS(self,debit=1):
		w, created = Wallet.activeObjects.get_or_create(owner = self.owner)
		if w.smsCredits:
			w.smsCredits = w.smsCredits-debit
			w.save()
			return True
		else :
			return False
		
	def saveStatus(self,status, reason, responceTime):
		try:
			status = int(status)
		except:
			status = None
			if not reason:
				reason = "Unknown Status"
		ls = LadduStatus(laddu=self, status=status, statusReason=reason, responceTimeSeconds = responceTime)
		ls.save()
		#print reason
		if reason == "('Connection aborted.', gaierror(-2, 'Name or service not known'))":
			reason = "Name or service not known"
			
		gElement = [{
			'x' : (int(time.time()))*1000,
			'y' : responceTime if responceTime else 0,
			'testStatus' : {
				'code' : status if status else 'Unknown',
				'reason' : reason,
				}
			},]
		if not GRAPH_DB.find({'laddu':self.id}).count():
			GRAPH_DB.insert({'laddu':self.id, 'graph':gElement})
		else:
			gElement = GRAPH_DB.find_one({'laddu':self.id})['graph'] + gElement
			GRAPH_DB.update({'laddu':self.id},{"$set":{'graph':gElement}})
			
		ls, created = LadduStatics.objects.get_or_create(laddu = self)
		
		### ALERT
		if ( not status ) or ( status in range(400,600) ):
			self.doAlert(status, reason)
			ls.failCount = 1 if not ls.failCount else (ls.failCount + 1)
		else:
			ls.successCount = 1 if not ls.successCount else (ls.successCount + 1)
		ls.save()
	
	def getGraph(self):
		if GRAPH_DB.find({'laddu':self.id}).count():
			gElement = GRAPH_DB.find_one({'laddu':self.id})['graph']
			return gElement
		else:
			return None
	
	def getSuccessCount(self):
		ls = None
		try : 
			ls = LadduStatics.objects.get(laddu = self)
		except:
			pass
		return ls.successCount if ls and ls.successCount else 0
	
	def getFailCount(self):
		ls = None
		try : 
			ls = LadduStatics.objects.get(laddu = self)
		except:
			pass
		return ls.failCount if ls and ls.failCount else 0


class LadduStatus(models.Model):
	laddu = models.ForeignKey(Laddu)
	
	status = models.PositiveSmallIntegerField(null=True,blank=True)
	statusReason = models.TextField(null=True, blank=True)
	responceTimeSeconds = models.FloatField(null=True, blank=True)
	
	active = models.BooleanField(default=True)
	addedOn = models.DateTimeField(auto_now_add=True)
	UpdatedOn = models.DateTimeField(auto_now=True)


class SmsAlert(models.Model):
	laddu = models.ForeignKey(Laddu)
	number = models.TextField()
	
	active = models.BooleanField(default=True)
	addedOn = models.DateTimeField(auto_now_add=True)
	UpdatedOn = models.DateTimeField(auto_now=True)
	
	objects = models.Manager()
	activeObjects = ActiveObjects()
	

class EmailAlert(models.Model):
	laddu = models.ForeignKey(Laddu)
	email = models.TextField()
	
	active = models.BooleanField(default=True)
	addedOn = models.DateTimeField(auto_now_add=True)
	UpdatedOn = models.DateTimeField(auto_now=True)
	
	objects = models.Manager()
	activeObjects = ActiveObjects()

class LadduStatics(models.Model):
	laddu = models.ForeignKey(Laddu, primary_key=True)
	successCount = models.PositiveSmallIntegerField(null=True,blank=True)
	failCount = models.PositiveSmallIntegerField(null=True,blank=True)	
	def getSuccessPercent(self):
		return ( self.successCount / ( self.successCount + self.failCount ) ) * 100


class UserStatics(models.Model):
	user = models.ForeignKey(User, primary_key=True)
	def getRunningJobsCount(self):
		return Laddu.objects.filter(runningStatus=True).count()

class UserForm(ModelForm):
	class Meta:
		model = User
		fields = ('username', 'email', 'password')
		
class Secretkey(models.Model):
	username = models.CharField(max_length=40)
	key = models.CharField(max_length=20)
	active = models.BooleanField(default=True)
	

from datetime import timedelta
class userExtension:
	user = None
	laddus = None
	
	def __init__(self,u):
		self.user = u
		self.laddus = Laddu.activeObjects.filter(owner = self.user)
		
	def runningJobCount(self):
		return self.laddus.count()
	
	def weekJobCount(self):
		
		date = datetime.today()
		start_week = date - timedelta(date.weekday())
		end_week = start_week + timedelta(7)
		return LadduStatus.objects.filter(laddu__in= self.laddus, UpdatedOn__range=[start_week, end_week]).count()
	
	def failedChecks(self):
		date = datetime.today()
		start_week = date - timedelta(date.weekday())
		end_week = start_week + timedelta(7)
		fLcount = LadduStatus.objects.filter(laddu__in= self.laddus, UpdatedOn__range=[start_week, end_week], status__range=[400,600]).count()
		fLcount += LadduStatus.objects.filter(laddu__in= self.laddus, UpdatedOn__range=[start_week, end_week], status=None ).count()
		return fLcount
	
	def smsCreditsRemaing(self):
		w, created = Wallet.activeObjects.get_or_create(owner = self.user)
		return w.smsCredits