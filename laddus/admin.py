from django.contrib import admin
from laddus.models import Laddu, LadduStatus, SmsAlert,UserForm, EmailAlert, Wallet
# Register your models here.




class LadduAdmin(admin.ModelAdmin):
	 list_display = ('protocol', 'url','owner','runningStatus', 'frequency', 'active')

admin.site.register(Laddu,LadduAdmin)

admin.site.register(LadduStatus)
admin.site.register(SmsAlert)
admin.site.register(EmailAlert)
admin.site.register(Wallet)
