# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('laddus', '0005_auto_20141102_1734'),
    ]

    operations = [
        migrations.AlterField(
            model_name='laddu',
            name='runningStatus',
            field=models.NullBooleanField(default=True, choices=[(True, b'Runing'), (False, b'Paused'), (None, b'Stopped')]),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='laddustatus',
            name='status',
            field=models.PositiveSmallIntegerField(null=True, blank=True),
            preserve_default=True,
        ),
    ]
