# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('laddus', '0004_laddustatus_statusreason'),
    ]

    operations = [
        migrations.AddField(
            model_name='laddu',
            name='protocol',
            field=models.CharField(default=b'http://', max_length=10, choices=[(b'http://', b'http://'), (b'https://', b'https://')]),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='laddu',
            name='url',
            field=models.TextField(),
            preserve_default=True,
        ),
    ]
