# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('laddus', '0007_auto_20141112_1046'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='laddustatus',
            name='responceTime',
        ),
    ]
