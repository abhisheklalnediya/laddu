# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('laddus', '0002_auto_20141102_0532'),
    ]

    operations = [
        migrations.AddField(
            model_name='laddustatus',
            name='active',
            field=models.BooleanField(default=True),
            preserve_default=True,
        ),
    ]
