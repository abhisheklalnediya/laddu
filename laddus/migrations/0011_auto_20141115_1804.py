# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('laddus', '0010_auto_20141115_0709'),
    ]

    operations = [
        migrations.CreateModel(
            name='LadduStatics',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('successCount', models.PositiveSmallIntegerField(null=True, blank=True)),
                ('failCount', models.PositiveSmallIntegerField(null=True, blank=True)),
                ('laddu', models.ForeignKey(to='laddus.Laddu')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.RenameModel(
            old_name='Statics',
            new_name='UserStatics',
        ),
    ]
