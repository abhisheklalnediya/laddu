# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        ('laddus', '0011_auto_20141115_1804'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='laddustatics',
            name='id',
        ),
        migrations.RemoveField(
            model_name='userstatics',
            name='id',
        ),
        migrations.AlterField(
            model_name='laddustatics',
            name='laddu',
            field=models.ForeignKey(primary_key=True, serialize=False, to='laddus.Laddu'),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='userstatics',
            name='user',
            field=models.ForeignKey(primary_key=True, serialize=False, to=settings.AUTH_USER_MODEL),
            preserve_default=True,
        ),
    ]
