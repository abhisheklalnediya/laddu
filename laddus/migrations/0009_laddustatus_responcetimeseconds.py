# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('laddus', '0008_remove_laddustatus_responcetime'),
    ]

    operations = [
        migrations.AddField(
            model_name='laddustatus',
            name='responceTimeSeconds',
            field=models.FloatField(null=True, blank=True),
            preserve_default=True,
        ),
    ]
