# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('laddus', '0006_auto_20141102_1827'),
    ]

    operations = [
        migrations.AddField(
            model_name='laddustatus',
            name='responceTime',
            field=models.TextField(null=True, blank=True),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='laddu',
            name='frequency',
            field=models.PositiveIntegerField(default=120, choices=[(15, b'15 Minutes'), (30, b'30 Minutes'), (60, b'1 Hour'), (120, b'2 Hour'), (720, b'12 Hour'), (1440, b'Day(01:00 IST)')]),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='laddu',
            name='runningStatus',
            field=models.NullBooleanField(default=True, choices=[(True, b'Running'), (False, b'Paused'), (None, b'Stopped')]),
            preserve_default=True,
        ),
    ]
