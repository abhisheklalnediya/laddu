function setStatus(jobStatus, btn)
{
	if (jobStatus == true)
	{
		$(btn).parents('.jobRow').removeClass('stopped')
		$(btn).parents('.jobRow').addClass('running')
	}
	else
	{
		$(btn).parents('.jobRow').addClass('stopped')
		$(btn).parents('.jobRow').removeClass('running')
	}
}
$(document).on('click', '.jobStop,.jobStart', function(e) {
	e.preventDefault();
	var btn = $(this);
	if (btn.hasClass('disabled'))
		return false;
	$(btn).addClass('disabled')
	$.post($(this).attr('href'),
		{
			csrfmiddlewaretoken: $('#csrf_token').val()
		}, function(data) {
		setStatus(data.jobStatus, btn)
	}).fail(function() {

	}).always(function() {
		$(btn).removeClass('disabled')
	})
		;
});

$(document).on('click', '.jobRemove', function(e) {
	e.preventDefault();
	var btn = $(this);
	if (btn.hasClass('disabled'))
		return false;
	$(btn).addClass('disabled')
	$.post($(this).attr('href'),
		{
			csrfmiddlewaretoken: $('#csrf_token').val()
		}, function(data) {
		$(btn).parents('.jobLi').slideUp().queue(function(){
			$(btn).parents('.jobLi').remove();
		});
		
	}).fail(function() {

	}).always(function() {
		$(btn).removeClass('disabled')
	})
		;
});

