function graphify(gC, data, url) {
	var color = ["#7cb5ec", "#ff3c50", "#434348", "#90ed7d", "#f7a35c", "#8085e9", "#e4d354", "#8085e8", "#8d4653", "#91e8e1"]
	if (data) {
		for (i = 0; i < data.length; i++)
		{
			var colorIndex = 0
			var code = data[i].testStatus.code
			if (isNaN(code))
			{
				colorIndex = 1
			}
			else if (code >= 400)
			{
				colorIndex = 1
			}
			data[i].color = {
				linearGradient: {x1: 0, x2: 0, y1: 1, y2: 0},
				stops: [
					[0, color[colorIndex]],
					[1, Highcharts.Color(color[colorIndex]).brighten(-0.3).get('rgb')] // darken
				]
			}
			data[i].marker = {fillColor: '#BF0B23', radius: 10}
		}


		$(gC).highcharts({
			chart: {
				type: 'column'
			},
			credits: {
				enabled: false
			},
			title: {
				text: 'Monitor Report',
				style: {"display": "none"}

			},
			subtitle: {
				text: 'Url : ' + url,
				style: {"display": "none"}
			},
			legend: {
				enabled: false
			},
			xAxis: {
				type: 'datetime',
				dateTimeLabelFormats: {
					second: '%H:%M:%S  %e.%b',
					minute: '%H:%M %e.%b',
					hour: '%H:%M %e.%b',
					day: '%e.%b',
					week: '%e.%b',
					month: '%m \'%y'
				},
				title: {
					text: 'Date',
					enabled: false
				},
				labels: {
					enabled: false
				},
				gridLineWidth: 0


			},
			yAxis: {
				title: {
					text: 'Responce Time(s)',
					enabled: false
				},
				min: 0,
				labels: {
					enabled: false
				},
				gridLineWidth: 0
			},
			exporting: {
				enabled: false
			},
			tooltip: {
				enabled: false,
				useHTML: true,
				formatter: function() {

					var time = new Date(this.x);
					var timeStr = time.getDate() + '.' + time.getMonth() + '.' + time.getFullYear() + ' ' + time.getHours() + ':' + time.getMinutes()
					var t = "";
					t += "" + timeStr + "\n";
					t += "" + data[this.point.index].testStatus.code + ' ' + data[this.point.index].testStatus.reason + "";
					t += "";
					return t
				}
			},
			series: [{
					name: 'Check Url',
					data: data,
					turboThreshold: 100000
				}]
		});
	}
}