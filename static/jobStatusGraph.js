function graphify_FULL(gC, data, url, sp, fp) {
	if (data == null)
		data = []
	var color = ["#7cb5ec", "#ff3c50", "#434348", "#90ed7d", "#f7a35c", "#8085e9", "#e4d354", "#8085e8", "#8d4653", "#91e8e1"]

	for (i = 0; i < data.length; i++)
	{
		var colorIndex = 0
		var code = data[i].testStatus.code
		if (isNaN(code))
		{
			colorIndex = 1
		}
		else if (code >= 400)
		{
			colorIndex = 1
		}
		data[i].color = {
			radialGradient: {cx: 0.5, cy: 0.3, r: 0.7},
			stops: [
				[0, color[colorIndex]],
				[1, Highcharts.Color(color[colorIndex]).brighten(-0.3).get('rgb')] // darken
			]
		}
	}



	// Radialize the colors
	Highcharts.getOptions().colors = Highcharts.map(color, function(color) {
		return {
			radialGradient: {cx: 0.5, cy: 0.3, r: 0.7},
			stops: [
				[0, color],
				[1, Highcharts.Color(color).brighten(-0.3).get('rgb')] // darken
			]
		};
	});
	if (data) {
		$(gC).highcharts({
			chart: {
				type: 'column',
				zoomType: 'x'
			},
			credits: {
				enabled: false
			},
			title: {
				text: 'Monitor Report',
				style: {"display": "none"}

			},
			subtitle: {
				text: 'Url : ' + url,
				style: {"display": "none"}
			},
			legend: {
				enabled: false
			},
			xAxis: {
				type: 'datetime',
				dateTimeLabelFormats: {
					second: '%H:%M:%S  %e.%b',
					minute: '%H:%M',
					hour: '%H:%M',
					day: '%e.%b',
					week: '%e.%b',
					month: '%m \'%y'
				},
				title: {
					text: 'Date',
					enabled: false
				},
				labels: {
//					enabled: false
				},
				max : new Date().getTime()

			},
			yAxis: {
				title: {
					text: 'Responce Time(s)',
//					enabled: false
				},
				min: 0,
				labels: {
//					enabled: false
				}
			},
			exporting: {
				enabled: false
			},
			tooltip: {
				useHTML:true,
				formatter: function() {
					
					if (this.series.name == "Success Percent")
					{
						var t = "";
						t += "" + this.key + " : " + this.percentage.toFixed(2) + '%';
						
						return t
						
					}
					else
					{
						var time = new Date(this.x );
						var timeStr = time.getDate() + '.' + time.getMonth() + '.' + time.getFullYear() + ' ' + time.getHours() + ':' + time.getMinutes()
						var t = "";
						t += "<b>" + timeStr + "</b><br>";
						t += "<b>" + data[this.point.index].testStatus.code + '</b> ' + data[this.point.index].testStatus.reason + " ";
						t += "";
						return t
					}
				}
			},
			plotOptions: {
				pie: {
					allowPointSelect: true,
					cursor: 'pointer',
					dataLabels: {
						enabled: true,
						format: '<b>{point.name}</b>: {point.percentage:.1f} %',
						style: {
							color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
						},
						connectorColor: 'silver'
					},
					startAngle: -90,
					endAngle: 90,
//				center: ['50%', '75%']
				}
			},
			series: [{
					name: 'Responce Time',
					data: data,
					turboThreshold: 100000
				},
				{
					type: 'pie',
					name: 'Success Percent',
					data: [{
							name: 'Up Time',
							y: sp
						}, {
							name: 'Down Time',
							y: fp
						}],
					center: [100, 50],
					size: 100,
					showInLegend: false,
					dataLabels: {
						enabled: true
					},
					tooltip: {
						enabled: false
					}
				}]
		});
	}
}