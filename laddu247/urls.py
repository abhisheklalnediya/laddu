from django.conf.urls import patterns, include, url
from django.contrib import admin
import laddus

urlpatterns = patterns('',

	url(r'^admin/', include(admin.site.urls)),
	
	url(r'^$', 'laddus.views.home', name='home'),
	
	url(r'^accounts/login/$', 'django.contrib.auth.views.login', {'template_name': 'login.html'},name='login_Page'),
	url(r'^accounts/logout/$', 'django.contrib.auth.views.logout_then_login'),
		      
	url(r'^accounts/profile/$', 'laddus.views.account_profile', name='account_profile'),
	url(r'^user/register$', 'laddus.views.userRegister', name='userRegister'),
	url(r'^user/save$', 'laddus.views.saveUser', name='saveUser'),

	url(r'^user/forgot/password$', 'laddus.views.forgotPasswordView', name='forgotPasswordView'),
	url(r'^user/pswd/send/mail$', 'laddus.views.forgotPassword', name='forgotPassword'),
	url(r'^reset/password/using$', 'laddus.views.resetPasswordUsingKey', name='resetPasswordUsingKey'),
	url(r'^save/new/password$', 'laddus.views.savePswdUsingKey', name='savePswdUsingKey'),
	url(r'^reset/password$', 'laddus.views.resetPassword', name='resetPassword'),
	url(r'^reset/password/save$', 'laddus.views.resetPasswordSave', name='resetPasswordSave'),

	url(r'^home/Submit$', 'laddus.views.homeSubmition', name='homeSubmition'),

	
	url(r'^jobs$', 'laddus.views.jobs', name='jobs'),
	url(r'^jobs/add$', 'laddus.views.addJob', name='newJob'),
	url(r'^job/(?P<id>[-\w]+)/$', 'laddus.views.jobStats', name='jobStats'),
	
	url(r'^job/start/(?P<id>[-\w]+)/$', 'laddus.views.jobStart', name='jobStart'),
	url(r'^job/stop/(?P<id>[-\w]+)/$', 'laddus.views.jobStop', name='jobStop'),
	url(r'^job/remove/(?P<id>[-\w]+)/$', 'laddus.views.jobRemove', name='jobRemove'),
	
	
	#SMS
	url(r'^job/add/smsAlert/(?P<id>[-\w]+)/$', 'laddus.views.jobAddSMSAlert', name='addSMSAlert'),
	url(r'^job/del/smsAlert/(?P<id>[-\w]+)/(?P<phoneNumber>[-\w]+)/$', 'laddus.views.jobDelSMSAlert', name='delSMSAlert'),
	
	# Email
	url(r'^job/add/emailAlert/(?P<id>[-\w]+)/$', 'laddus.views.jobAddEMAlert', name='addEMAlert'),
	url(r'^job/del/emailAlert/(?P<id>[-\w]+)/(?P<email>[\w.%+-]+@[A-Za-z0-9.-]+\.[A-Za-z]{2,})/$', 'laddus.views.jobDelEMAlert', name='delEMAlert'),
	

)
