# -*- coding: utf-8 -*-
import celery
from celery.task.schedules import crontab
from celery.decorators import task
import datetime
import httplib
from laddus.models import Laddu
import requests

@task()
def checkLaddu(laddu):
	
	try:
		print "checking :"
		print str(laddu.pk) + laddu.protocol + laddu.url
		req = requests.request('GET', laddu.protocol + laddu.url)
		laddu.saveStatus(req.status_code, req.reason, req.elapsed.total_seconds())
	except Exception,e:
		laddu.saveStatus(None, str(e),0)


@celery.decorators.periodic_task(run_every=datetime.timedelta(minutes=1))
def doLaddu5():
	print "15Min Check Started"
	ls = Laddu.activeObjects.filter(frequency=15)
	for l in ls:
		checkLaddu.delay(l)


@celery.decorators.periodic_task(run_every=datetime.timedelta(minutes=30))
def doLaddu30():
	print "30Min Check Started"
	ls = Laddu.activeObjects.filter(frequency=30)
	for l in ls:
		checkLaddu.delay(l)

@celery.decorators.periodic_task(run_every=datetime.timedelta(minutes=60))
def doLaddu60():
	print "60Min Check Started"
	ls = Laddu.activeObjects.filter(frequency=60)
	for l in ls:
		checkLaddu.delay(l)



@celery.decorators.periodic_task(run_every=datetime.timedelta(minutes=120))
def doLaddu120():
	print "120Min Check Started"
	ls = Laddu.activeObjects.filter(frequency=120)
	for l in ls:
		checkLaddu.delay(l)



@celery.decorators.periodic_task(run_every=datetime.timedelta(minutes=720))
def doLaddu720():
	print "720Min Check Started"
	ls = Laddu.activeObjects.filter(frequency=720)
	for l in ls:
		checkLaddu.delay(l)


@celery.decorators.periodic_task(run_every=datetime.timedelta(minutes=1440))
def doLaddu1440():
	print "1440Min Check Started"
	ls = Laddu.activeObjects.filter(frequency=1440)
	for l in ls:
		checkLaddu.delay(l)

